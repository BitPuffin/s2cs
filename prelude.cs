#nullable enable

namespace S2CS;

public interface ISchemeValue
{
    object Value { get; }
}

public interface ISchemePrimitiveValue : ISchemeValue
{
}

